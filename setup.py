#! /usr/bin/env python

from setuptools import setup

setup(
    name='simpletotp',
    version='0.90',
    description='A simple TOTP library',
    author='Yoav Aner, Bertrand Frottier',
    author_email='Bertrand Frottier <bertrand.frottier@gmail.com>',
    packages=['simpletotp'],
    package_dir={'simpletotp': 'src/simpletotp'},
    entry_points={'console_scripts': ['google_totp = simpletotp:google_totp']},
)
