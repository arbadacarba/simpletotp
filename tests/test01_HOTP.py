import unittest
from simpletotp import HOTP


class HotpTest(unittest.TestCase):
    """
    a very simple test case for HOTP.
    Based on test vectors from http://www.ietf.org/rfc/rfc4226.txt
    """
    def setUp(self):
        self.key_string = '12345678901234567890'

    def test_hotp_vectors(self):
        """Simple test for HOTP"""
        hotp_result_vector = ['755224', '287082', '359152',
                              '969429', '338314', '254676',
                              '287922', '162583', '399871',
                              '520489', '060613']
        for i in range(0, 10):
            self.assertEquals(HOTP(self.key_string, i), hotp_result_vector[i])
