import unittest
from simpletotp import TOTP


class TotpTest(unittest.TestCase):
    """
    a very simple test case for TOTP.
    """
    def setUp(self):
        self.key_string = '12345678901234567890'

    def test_totp(self):
        """Simple test for TOTP

        since TOTP depends on the time window, we cannot predict the value.
        However, if we execute it several times, we should expect the
        same response most of the time.
        We only expect the value to change
        once or not at all within a reasonable time window."""
        value = TOTP(self.key_string, digits=8, window=20)
        value_changes = 0  # counting the number of changes to TOTP value
        for i in range(0, 100000):
            new_totp = TOTP(self.key_string, digits=8, window=20)
            if new_totp != value:
                value_changes += 1
                value = new_totp
        self.assertTrue(value_changes <= 1)
